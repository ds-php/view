<?php
namespace Tests\Ds\View\TemplateEngine;

use Ds\View\TemplateEngine\TwigEngine;

/**
 * Class Twig1xTest
 * @package Tests\Ds\View\TemplateEngine
 */
class TwigEngineTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var
     */
    public $twigLoader;
    /**
     * @var
     */
    public $twigEnv;

    /**
     * @var TwigEngine
     */
    public $twigEngine;

    /**
     *
     */
    public function setUp() : void
    {

       
        $this->twigLoader = $this->getMockBuilder('\Twig\Loader\LoaderInterface')
        ->getMock();
        $this->twigEnv = $this->getMockBuilder('\Twig\Environment')
        ->disableOriginalConstructor()    
        ->getMock();
        $this->twigEngine = new \Ds\View\TemplateEngine\TwigEngine($this->twigLoader, $this->twigEnv);
    }


    function rr(){
        $loader = new \Twig\Loader\ArrayLoader([
            'index' => 'Hello {{ name }}!',
        ]);
        $twig = new \Twig\Environment($loader);
        
        echo $twig->render('index', ['name' => 'Fabien']);
        

    }


 
    /**
     *
     */
    public function testGetLoader()
    {
        $loader = $this->twigEngine->getLoader();
        $this->assertSame($this->twigLoader, $loader);
    }

    /**
     *
     */
    public function testGetEnvironment()
    {
        $loader = $this->twigEngine->getEnvironment();
        $this->assertSame($this->twigEnv, $loader);
    }

    /**
     *
     */
    public function testWithLoader()
    {
        $loader = new \Twig\Loader\ArrayLoader([
            'index' => 'Hello {{ name }}!',
        ]);

   
        $view = $this->twigEngine->withLoader($loader);
        $this->assertNotSame($this->twigLoader, $view->getLoader());
    }

    /**
     *
     */
    public function testWithEnvironment()
    {
        $twigEnv = $this->getMockBuilder('\Twig\Environment')
        ->disableOriginalConstructor()
        ->getMock();
        $view = $this->twigEngine->withEnvironment($twigEnv);
        $this->assertNotSame($this->twigEnv, $view->getEnvironment());
    }
}
