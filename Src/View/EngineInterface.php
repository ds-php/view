<?php

namespace Ds\View;

/**
 * Interface EngineInterface
 * @package Ds\View
 */
interface EngineInterface
{
    /**
     * Get template output.
     * @param string $path Path to template.
     * @param array $data Data to be passed to template.
     * @param array $options TemplateEngine render options.
     * @return string
     */
    public function render($path, array $data = [], array $options = []);

    
}
