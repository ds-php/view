<?php

namespace Ds\View\TemplateEngine;

use Ds\View\EngineInterface;


/**
 * Class TwigEngine
 *
 * Updated Template Engine for Twig 3.0.
 *
 * @package Ds\View\TemplateEngine
 */
class TwigEngine implements EngineInterface
{

    /**
     * @var \Twig\Loader\LoaderInterface
     */
    public $loader;

    /**
     * @var \Twig\Environment
     */
    public $environment;

    /**
     * Twig constructor.
     *
     * @param \Twig\Loader\LoaderInterface $loader Twig Loader
     * @param \Twig\Environment $environment Twig Environment
     */
    public function __construct(
        \Twig\Loader\LoaderInterface $loader,
        \Twig\Environment $environment
    )
    {
        $this->loader = $loader;
        $this->environment = $environment;
    }

    /**
     * Render template.
     *
     * @param string $path Path to template.
     * @param array $data Data to be passed to template.
     * @param array $options TemplateEngine render options
     *
     * @return string
     */
    public function render($path, array $data = [], array $options = [])
    {
        $cacheView = $options['cache'] ?? false;
        $renderBlock = $options['block'] ?? false;

        $twigCacheDirectory = $this->environment->getCache();

        $cacheFilesystem = $this->getTwigCacheFilesystem();

        $templateCacheName = $this->loader->getCacheKey();

        if (!$cacheView || !$twigCacheDirectory) {
            $this->environment->setCache(false);
            $this->clearCacheFile($templateCacheName);
        }

        $template = $this->environment->load($path);

        if ($renderBlock) {
            return (string)$template->renderBlock($options['block'], $data);
        }

        return (string)$template->render($data);
    }

    /**
     * Get Twig Cache Filesystem.
     *
     * @return \Twig_Cache_Filesystem
     */
    public function getTwigCacheFilesystem()
    {
        $reflector = new \ReflectionClass('\Twig\Environment');
        $prop = $reflector->getProperty('cache');
        $prop->setAccessible(true);
        return $prop->getValue($this->environment);
    }

    /**
     * Clear Template Cache file.
     *
     * @param string $filename
     */
    public function clearCacheFile(string $filename)
    {
        if (\file_exists($filename)) {
            \unlink($filename);
        }
    }

    /**
     * Get Cache template created Timestamp.
     *
     * @param \Twig_Cache_Filesystem $cacheFilesystem Twig Cache Filesystem.
     * @param string $path Cache template path.
     *
     * @return int
     */
    public function getCacheCreatedTime(\Twig_Cache_Filesystem $cacheFilesystem, $path)
    {
        return $cacheFilesystem->getTimestamp($path);
    }

  

    /**
     * Get Twig Loader
     *
     * @return \Twig\Loader\LoaderInterface
     */
    public function getLoader()
    {
        return $this->loader;
    }

    /**
     * Get Twig Environment
     *
     * @return \Twig\Environment
     */
    public function getEnvironment()
    {
        return $this->environment;
    }

    /**
     * With Twig Loader.
     *
     * @param \Twig\Loader\LoaderInterface $loader
     * @return TwigEngine
     */
    public function withLoader(\Twig\Loader\LoaderInterface $loader)
    {
        $new = clone $this;
        $new->loader = $loader;
        return $new;
    }

    /**
     * With Twig Environment.
     *
     * @param \Twig\Environment $environment
     * @return TwigEngine
     */
    public function withEnvironment(\Twig\Environment $environment)
    {
        $new = clone $this;
        $new->environment = $environment;
        return $new;
    }
}
